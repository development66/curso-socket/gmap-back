import { Marcador } from "./marcador";

export class Mapa {
    public marcadores: Marcador[] = [];

    constructor() {}

    getMarcadores() {
        return this.marcadores;
    }

    agregarMarcador(marcador: Marcador) {
        this.marcadores = [...this.marcadores, marcador];
    }

    borrarMarcador(id: string) {
        this.marcadores = this.marcadores.filter(mark => mark.id !== id);
        return this.getMarcadores();
    }

    moverMarcador(marcador: Marcador) {
        for (const i in this.marcadores) {
            if (this.marcadores[i].id === marcador.id) {
                this.marcadores[i].lng = marcador.lng;
                this.marcadores[i].lat = marcador.lat;
                return;
            }
        }
    }
}