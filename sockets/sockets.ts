import { Socket } from 'socket.io';
import { UsuariosLista } from '../classes/usuarios-lista';
import { Usuario } from '../classes/usuario';
import { mapa } from '../routes/router';

export const usuariosConectados = new UsuariosLista();

export const socketMarcador = (cliente: Socket) => {
    cliente.on('marcador-nuevo', (marcador) =>  {
        mapa.agregarMarcador(marcador);
        cliente.broadcast.emit('marcador-nuevo', marcador);
    });

    cliente.on('marcador-borrar', (id) =>  {
        mapa.borrarMarcador(id);
        cliente.broadcast.emit('marcador-borrar', id);
    });

    cliente.on('marcador-mover', (marcador) =>  {
        mapa.moverMarcador(marcador);
        cliente.broadcast.emit('marcador-mover', marcador);
    });
}

// export const mapa = new Mapa();

// export const mapaSockets = (cliente: Socket, io : SocketIO.Server) => {
//     cliente.on('marcador-nuevo', (marcador: Marcador) => {
//         mapa.agregarMarcador(marcador);
//         cliente.broadcast.emit('marcador-nuevo', marcador);
//     });

//     cliente.on('marcador-borrar', (id: string) => {
//         mapa.borrarMarcador(id);
//         cliente.broadcast.emit('marcador-borrar', id);
//     });

//     cliente.on('marcador-mover', (marcador: Marcador) => {
//         mapa.moverMarcador(marcador);
//         cliente.broadcast.emit('marcador-mover', marcador);
//     });
// }

export const conectarCliente = (cliente: Socket) => {
    const usuario = new Usuario(cliente.id);
    usuariosConectados.agregar(usuario);
}

export const desconectar = (cliente: Socket) => {
    cliente.on('disconnect', () => {
        console.log('Cliente Desconectado');
        const user = usuariosConectados.borrarUsuario(cliente.id);
        console.log('Borrar user ', user?.id, user?.nombre);
    });
}

// Listen message
export const mensaje = (Cliente: Socket, io: SocketIO.Server) => {
    Cliente.on('mensaje', (payload: { de: string, cuerpo: string }) => {
        console.log('Mensaje recibido ', payload);
        io.emit('mensaje-nuevo', payload);
    });
}

// configurar usuario 
export const configurarUsuario = (cliente: Socket, io: SocketIO.Server) => {
    cliente.on('configurar-usuario', (payload: { nombre: string }, callback: Function) => {
        // io.emit('configurar-usuario', payload);
        usuariosConectados.actualizarNombre(cliente.id, payload.nombre);
        callback({
            ok: true,
            mensaje: `Usuario ${payload.nombre} configurado`
        });
    }); 
}